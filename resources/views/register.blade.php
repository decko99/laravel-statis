<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
     
    <div>
        <h1>Buat Account</h1>
        <h2>Sign Up Form</h2>
    </div>

    <div>
        <form action="/post" method="POST"> 
            @csrf
            <div id="textName">
                <label for="first_name">First name: </label>
                <br>
                    <input type="text" name="first_name" id="first_name">
                <br><br>
                <label for="last_name">Last name: </label>
                <br>
                    <input type="text" name="last_name" id="last_name">
                <br><br>
            </div>
            
            <div id="radioGender">
                <label>Gender:</label>
                <br>
                    <input type="radio" name="gender" id="male" name="male" value="male"> 
                    <label for="male">Male</label>
                <br>
                    <input type="radio" name="gender" id="female" name="female" value="Female">
                    <label for="female">Female</label>
                <br><br>
            </div>

            <div id="listNation">
                <label>Nationality: </label> 
                <br>
                    <select name="nationality">
                        <option value="Indonesian">Indonesian</option>
                        <option value="Malaysian">Malaysian</option>
                        <option value="English">English</option>
                        <option value="Others">Others</option>
                    </select>
                <br><br>
            </div>
            
            <div id="cbLanguage">
                <label>Language Spoken: </label>
                <br>
                    <input type="checkbox" id="bahasa" name="bahasa" value="bahasa">
                    <label for="bahasa">Bahasa Indonesia</label>
                <br>
                    <input type="checkbox" id="english" name="english" value="english">
                    <label for="english">English</label>
                <br> <br>
            </div>

            <div id="textareaBio">
                <label>Bio: </label>
                <br>
                    <textarea name="bio" rows="10" cols="40" placeholder="Isikan Biodata..."></textarea>
            </div>
            <br>
            <input type="submit" value="Sign Up">
        </form>
    </div>
</body>
</html>